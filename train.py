from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import LSTM
from keras.layers import BatchNormalization
from keras.layers import Conv1D, MaxPooling1D
from keras.models import model_from_json
from keras.callbacks import ModelCheckpoint

import matplotlib.pyplot as plt

import os
import numpy as np
import string
import tensorflow as tf

os.environ["CUDA_VISIBLE_DEVICES"] = "3"  # it means 0 #delete incase u r doing on CPU

alpha_to_num = dict(zip(string.ascii_lowercase, range(1, 27)))

#####################################################
### Loading data ####################################
#####################################################
f = open('xtrain_obfuscated.txt', 'r')
txt = f.readlines()

total_size = len(txt)
x_train = np.zeros((total_size, 453), dtype=np.int)

for i in range(len(txt)):
    x = np.zeros(453, dtype=np.int)
    for j in range(453):
        if j < len(txt[i]) - 1:
            x[j] = alpha_to_num[txt[i][j]]
        else:
            x[j] = 0
    x_train[i] = np.copy(x)
    
f = open('ytrain.txt', 'r')
txt = f.readlines()

y_train = np.zeros((total_size, 12), dtype=np.int)

for i in range(len(txt)):
    y_train[i][int(txt[i])] = 1
########################################################    

######################################################
############ paramters to play with ##################
######################################################
# Embedding
max_features = 26
maxlen = 453
embedding_size = 13

# Convolution
kernel_size = 5
filters = 64
pool_size = 4

# LSTM
lstm_output_size = 100

# Training
batch_size = 1000
epochs = 250
#######################################################



#######################################################
######### Building Model###############################
#######################################################
print('Building the model')

model = Sequential()
# 1000X453X26 #actualy the input is 1000X453 with every element having value from 1 to 26
model.add(Embedding(max_features, embedding_size, input_length=maxlen))
# 1000X453X13
model.add(Dropout(0.20))#randomly makes 20% of inputs as zero

model.add(BatchNormalization())#normalizes the input value for the batch with zero mean and unit variance, leads to faster convergence , better training. solves gradient overshooting problem
#num Filters = 64 filter dimension 5X1 stride 1  filter will have 13 depth
#1000X453X13
model.add(Conv1D(filters,
                 kernel_size,
                 padding='valid',
                 activation='relu',
                 strides=1))
#1000X449X64
model.add(MaxPooling1D(pool_size=pool_size))
#1000X113X64
model.add(LSTM(lstm_output_size)) # return sequence = false return only the last time step
#1000X100
model.add(BatchNormalization())#normalize the previous values with zero mean and unit variance
model.add(Dense(500))
#1000X500
model.add(BatchNormalization())
model.add(Dense(12))
#1000X12
model.add(Activation('sigmoid'))
#######################################################


######################################################
########## Compile Model #############################
######################################################
######### Catgorical cross entropy for multi class ###
######### Binary cross entropy for two class #########
######################################################
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
######################################################


#####################################################
#### Checkpoint (will save only best) ###############
#####################################################
filepath="weights.best.hdf5"
#checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]
#####################################################


#####################################################
########### Training ################################
#####################################################
print('Training the model')
history = model.fit(x_train, y_train, validation_split=0.1, 
                    epochs=epochs, batch_size=batch_size,callbacks=callbacks_list)
#####################################################


#################################################
###### serialize model to JSON ##################
#################################################
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
#################################################

#################################################
######## Plotting the results ###################
#################################################
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
##################################################

###############################
#### THE END ##################
###############################
