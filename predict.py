from keras.models import model_from_json

import numpy as np
import string
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "3"# it means 0

alpha_to_num = dict(zip(string.ascii_lowercase, range(1, 27)))

##################################
#read data and padd it accordingly
##################################
f = open('xtest_obfuscated.txt', 'r')
txt = f.readlines()
test_size = len(txt)

x_test = np.zeros((test_size, 453), dtype=np.int)
for i in range(test_size):
    x = np.zeros(453, dtype=np.int)
    for j in range(453):
        if j < len(txt[i]) - 1:
            x[j] = alpha_to_num[txt[i][j]]
        else:
            x[j] = 0
        x_test[i] = np.copy(x)

############## Data Loaded ########
    
############################
# load json and create model
############################
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

#############################
# load weights into new model
#############################
loaded_model.load_weights("weights.best.hdf5")
print("Loaded model from disk")
 
#################################### 
# evaluate loaded model on test data
####################################
loaded_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
pred = loaded_model.predict(x_test)

####################################
# write to file
####################################
y_test = np.zeros(test_size, dtype=np.int)
for i in range(x_test.shape[0]):
    y_test[i] = np.argmax(pred[i])
np.savetxt('ytest.txt', [y_test],fmt='%i', delimiter='\n')

######################
#### THE END #########
######################

